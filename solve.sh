#!/bin/bash

infile="$1"

tmpdir="$(mktemp -d /tmp/solve.$(date +%Y-%m-%d_%H:%M:%S).XXXXXXXXXX)"
script_dir="$(realpath $(dirname -- ${BASH_SOURCE[0]}))"

echo "working directory: $tmpdir"

cp "$infile" "$tmpdir"

baseinfile="$(basename -- $infile)"

cd "$tmpdir"

solve-field --scale-low .5 --scale-high 2 "./$baseinfile"

annotations=$(plot-constellations -w *.wcs -D -d /usr/share/astrometry/data/hd.fits -o "$baseinfile.annotation.png")
echo "$annotations"

magick composite "$baseinfile.annotation.png" *-ngc.png "$baseinfile.annotated.png"

ra=$(cat *.wcs | sed -re 's/(.{80})/\1\n/g' | head -50 | grep CRVAL1 | awk '{print $3}')
dec=$(cat *.wcs | sed -re 's/(.{80})/\1\n/g' | head -50 | grep CRVAL2 | awk '{print $3}')

"$script_dir"/nearest "$ra" "$dec"

display "$baseinfile.annotated.png" &

echo "working directory: $tmpdir"
