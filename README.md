astrotools
======

## About
This is a collection of tools I've made to support my astrophotography.

## Tools

### solve.sh
1. identifies the ra/dec of the image using solve-field
2. identifies stars from the henry draper catalog in the frame with plot-constellations
3. uses imagemagic to make a overlay the annotations from plot-constellations on the original image
4. uses my `nearest` tool to identify the nearest henry draper star to the center of the image

### nearest
1. given an ra/dec, reports the nearest henry draper star
    1. uses haversine to calculate spherical distance both for accuracy and to avoid gimbal lock issues near the poles
	2. you will have to run `make` to build this against your local astrometry.net libraries

## Dependencies
* astrometry.net offline tools
  * https://astrometry.net/doc/build.html
  * specifically installed such that gcc can find `-lcatalogs` and `-lastrometry`
* astrometry.net offline data installed to /usr/share/astrometry/data/
  * http://data.astrometry.net/4200/
  * get the files that cover the range of your imaging setup: https://astrometry.net/doc/readme.html#getting-index-files
    * if your imaging setup is enough bigger or smaller than mine, you may also want to change the scale (FOV) arguments to solve-field in solve.sh
* astrometry.net offline henry draper catalog installed to /usr/share/astrometry/data/
  * http://data.astrometry.net/hd.fits
* imagemagick
* realfile
* gcc
* make
