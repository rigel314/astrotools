#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <astrometry/hd.h>

double haversine(double dec1, double ra1, double dec2, double ra2);

int main(int argc, char *argv[])
{
	if (argc != 3)
	{
		printf("usage: ./nearest ra dec\n");
		return 1;
	}

	errno = 0;
	double ra = strtod(argv[1], NULL);
	double dec = strtod(argv[2], NULL);
	if (errno)
	{
		printf("invalid ra/dec\n");
		return 1;
	}

	hd_catalog_t *hdcat;

	hdcat = henry_draper_open("/usr/share/astrometry/data/hd.fits");

	if (!hdcat)
	{
		printf("failed reading hd catalog from '/usr/share/astrometry/data/hd.fits'\n");
		return 1;
	}

	double radius = 3600; // 1deg radius (in arcsec)
	while (1)
	{
		bl *nearby = henry_draper_get(hdcat, ra, dec, radius);

		int size = (int)bl_size(nearby);

		if (!size)
		{
			// really?  There was no HD star within a 1deg radius?  Try again with 2deg, etc
			radius += 3600;
			continue;
		}

		double mindist = INFINITY; // I guess I could use anything over M_PI
		int nearestHD = -1;
		// printf("%d\n", size);
		for (int i = 0; i < size; i++)
		{
			hd_entry_t hd;
			bl_get(nearby, i, &hd);
			double dist = haversine(0, 90, hd.ra, hd.dec);
			// double distsq = sqrt(pow(ra - hd.ra, 2) + pow(dec - hd.dec, 2));
			// printf("%6d: %+11.6f, %+9.6f, %+f, %+f\n", hd.hd, hd.ra, hd.dec, dist, distsq);
			if (dist < mindist)
			{
				nearestHD = hd.hd;
				mindist = dist;
			}
		}

		bl_free(nearby);

		printf("nearest star is HD %d\n", nearestHD);

		return 0;
	}
}

double haversine(double dec1, double ra1, double dec2, double ra2)
{
	double deg2rad = M_PI / 180.0;
	dec1 *= deg2rad;
	ra1 *= deg2rad;
	dec2 *= deg2rad;
	ra2 *= deg2rad;

	double dLat = dec2 - dec1;
	double dLon = ra2 - ra1;

	double a = pow(sin(dLat / 2), 2) + cos(dec1) * cos(dec2) * pow(sin(dLon / 2), 2);
	double c = 2 * asin(sqrt(a));

	return c;
}
